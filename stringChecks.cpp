// stringChecks.cpp
// Jericho Keyne
//
// This implents the functions from stringChecks.h
// This library just contains a large number of functions for
// checking the contents of strings and characters

#include <string>

using namespace std;

// Returns true if the character is upper case (in the range A to Z)
bool isUpperCase(char character) {
	return (character >= 'A' && character <= 'Z');
}

// Returns true if the character is lower case (in the range a to z)
bool isLowerCase(char character) { 
	return (character >= 'a' && character <= 'z');
}

// Returns true if the character is a letter (in the range A to Z or a to z, aka isUpperCase or isLowerCase)
bool isLetter(char character) {
	return (isUpperCase(character) || isLowerCase(character));
}

// Returns true if the character is a digit (in the range 0 to 9)
bool isDigit(char character) {
	return (character >= '0' && character <= '9');
}

// Returns true if the character is in the list of validCharacters
bool isValidSpecialCharacter(char character, string validCharacters) {
	for (int i = 0; i < validCharacters.length(); i++) {
		if (character == validCharacters[i])
			return true;
	}
	return false;
}

// Returns true if the password has at least one upper case letter	
bool containsUpperCase(string password) {
	for (int i = 0; i < password.length(); i++) {
		if (isUpperCase(password[i]))
			return true;
	}
	return false;
}

// Returns true if the password has at least one lower case letter
bool containsLowerCase(string password) {
	for (int i = 0; i < password.length(); i++) {
		if (isLowerCase(password[i]))
			return true;
	}
	return false;
}

// Returns true if the password has at least one digit
bool containsDigit(string password) {
	for (int i = 0; i < password.length(); i++) {
		if (isDigit(password[i]))
			return true;
	}
	return false;
}

// Returns true if the password has at least one valid special character
bool containsValidSpecialCharacters(string password, string validCharacters) {
	bool hasValidCharacter = false;
	for (int i = 0; i < password.length(); i++) {
		if (isValidSpecialCharacter(password[i], validCharacters))
			hasValidCharacter = true;
		if (!isLetter(password[i]) && !isDigit(password[i]) && !isValidSpecialCharacter(password[i], validCharacters))
			return false;
	}
	return hasValidCharacter;
}

// Returns true if the password has any invalid special characters
bool containsInvalidSpecialCharacters(string password, string validCharacters) {
	for (int i = 0; i < password.length(); i++) {
		if (!isLetter(password[i]) && !isDigit(password[i]) && !isValidSpecialCharacter(password[i], validCharacters))
			return true;
	}
	return false;
}

// Returns true if the password has any repeating letters
bool containsRepeatingLetters(string password) {
	for (int i = 0; i < password.length() - 1; i++) {
		if (isLetter(password[i]) && isLetter(password[i+1])) {
			if (password[i] == password[i+1])
				return true;
		}
	}
	return false;
}

// Returns true if the password has any repeating digits
bool containsRepeatingDigits(string password) {
	for (int i = 0; i < password.length() - 1; i++) {
		if (isDigit(password[i]) && isDigit(password[i+1])) {
			if (password[i] == password[i+1])
				return true;
		}
	}
	return false;
}

// Returns true if the password has any sequential digits
bool containsSequentialDigits(string password) {
	for (int i = 0; i < password.length() - 1; i++) {
		if (isDigit(password[i]) && isDigit(password[i+1])) {
			if (password[i+1] == password[i] + 1)
				return true;
		}
	}
	return false;
}
