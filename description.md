You are developing a software package that requires users to enter their own passwords. Your software requires that user passwords meet the following criteria:

1. the password should be at least 6 characters long and no more than 15 characters

2. the password should contain at least one uppercase and at at least one lowercase letter

3. the password should have at least one digit.

4. the password should contain one of the following characters: ! @ # $ % but no other special characters

Write a program that asks the user for a password and verifies that it meets the stated criteria.  If it doesn't, the program should display a message telling the user why. Your program should allow the user to try again if their password is not valid.

* User interface is important!

* You should post pseudocode

* Your code should have appropriate comments

* Your code should be well-formatted, with camel case variables

* Variables should be self-descriptive

* Your code should have functions (besides main!)

* Your functions should only do one thing

HINT: you should be passing pointers to functions
