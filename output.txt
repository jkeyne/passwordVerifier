The password is: bob
This password is too short
This password does not contain any upper case characters
This password does not contain any digits
This password does not contain any valid special characters

The password is: bobobbobobobobob
This password is too long
This password does not contain any upper case characters
This password does not contain any digits
This password does not contain any valid special characters
This password contains repeating letters

The password is: BOB
This password is too short
This password does not contain any lower case characters
This password does not contain any digits
This password does not contain any valid special characters

The password is: BOBOBBOB
This password does not contain any lower case characters
This password does not contain any digits
This password does not contain any valid special characters
This password contains repeating letters

The password is: bobBob
This password is too short
This password does not contain any digits
This password does not contain any valid special characters

The password is: bobBob123
This password does not contain any valid special characters
This password contains sequential digits

The password is: bobBob123$
This password contains sequential digits

The password is: bbobBob
This password is too short
This password does not contain any digits
This password does not contain any valid special characters
This password contains repeating letters

The password is: bobBob112$
This password contains repeating digits
This password contains sequential digits

The password is: obBob431*
This password does not contain any valid special characters
This password contains at least one invalid special character

The password is: bobBob14$
This password is valid

